const jwt = require('jsonwebtoken');
const config = require('../config/config.json');

module.exports = async (req, res, next) => {
  const { authorization: token } = req.headers;
  if (!token) {
    return res.status(401).json({
      success: false,
      message: 'Access denied, token is required',
    });
  }
  try {
    const decoded = await jwt.verify(token, config.tokenSecretKey);
    req.user = decoded;
    return next();
  } catch (err) {
    return res.status(400).json({
      success: false,
      message: 'Invalid Token',
    });
  }
};
