const express = require('express');

const router = express.Router();
const users = require('../controllers/users');

router.get('/:userId/carts', users.listCartsOfUser);
router.post('/', users.createUser);

module.exports = router;
