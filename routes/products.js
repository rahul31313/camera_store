const express = require('express');

const router = express.Router();
const products = require('../controllers/products');

router.get('/', products.listProducts);
router.get('/:productId', products.listProducts);
router.post('/', products.addProduct);

module.exports = router;
