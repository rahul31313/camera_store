const express = require('express');

const router = express.Router();
const categories = require('../controllers/categories');

router.get('/', categories.listCategories);
router.get('/:categoryId', categories.listCategories);
router.get('/:categoryId/products', categories.listProductsOfCategory);
router.post('/', categories.addCategory);

module.exports = router;
