const express = require('express');

const router = express.Router();
const categories = require('./categories');
const products = require('./products');
const users = require('./users');
const carts = require('./carts');
const login = require('../controllers/login');

router.use('/categories', categories);
router.use('/products', products);
router.use('/users', users);
router.use('/carts', carts);
router.post('/login', login.authenticateUser);

module.exports = router;
