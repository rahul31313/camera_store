const express = require('express');

const router = express.Router();
const carts = require('../controllers/carts');
const verifyToken = require('../middleware/verifyToken');

router.post('/', verifyToken, carts.addProcutsInCart);

module.exports = router;
