module.exports = (sequelize, DataTypes) => {
  const categories = sequelize.define('categories', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    type: {
      type: DataTypes.ENUM('Mirrorless', 'full frame', 'point and shoot'),
      allowNull: false,
    },
    model: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  });

  categories.associate = (models) => {
    categories.hasMany(models.products, {
      foreignKey: 'category_id',
      targetKey: 'id',
    });
  };

  return categories;
};
