module.exports = (sequelize, DataTypes) => {
  const products = sequelize.define('products', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    category_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    price: {
      type: DataTypes.DOUBLE,
      allowNull: false,
    },
    make: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  });

  products.associate = (models) => {
    products.belongsTo(models.categories, {
      foreignKey: 'category_id',
      targetKey: 'id',
    });
    products.hasMany(models.carts, {
      foreignKey: 'product_id',
      targetKey: 'id',
    });
  };

  return products;
};
