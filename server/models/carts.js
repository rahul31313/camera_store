module.exports = (sequelize, DataTypes) => {
  const carts = sequelize.define('carts', {
    product_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    quantity: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  });

  carts.associate = (models) => {
    carts.belongsTo(models.users, {
      foreignKey: 'user_id',
      targetKey: 'id',
    });
    carts.belongsTo(models.products, {
      foreignKey: 'product_id',
      targetKey: 'id',
    });
  };

  return carts;
};
