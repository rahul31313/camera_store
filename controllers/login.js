const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const models = require('../server/models');
const config = require('../config/config.json');

/**
 * authenticate user requires email and password
 * @param {*} req
 * @param {*} res
 */
const authenticateUser = async (req, res) => {
  const { email, password } = req.body || {};
  if (!email || !password) {
    return res.status(400).json({
      success: false,
      message: 'Both email and password required for login',
    });
  }
  try {
    let user = await models.users.findOne({
      where: {
        email,
      },
      attributes: ['password'],
    });
    if (!user) {
      return res.status(400).json({
        success: false,
        message: 'User does not exist',
      });
    }
    user = user.get({ plain: true });
    const { password: hashedPassword } = user;
    if (!(await bcrypt.compare(password, hashedPassword))) {
      return res.status('401').json({
        success: false,
        message: 'wrong credentials',
      });
    }
    const token = await jwt.sign({ email }, config.tokenSecretKey, {
      expiresIn: 60 * 60,
    });
    return res.status('200').json({
      token,
      success: true,
      message: 'User logged in successfully',
    });
  } catch (err) {
    return res.status(500).json({
      success: false,
      message: 'Some Error Occurred',
    });
  }
};
module.exports = {
  authenticateUser,
};
