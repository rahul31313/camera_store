const models = require('../server/models');

/**
 * adds product in cart for a user
 * @param {*} req
 * @param {*} res
 */
const addProcutsInCart = async (req, res) => {
  const { productId, quantity, userId } = req.body || {};
  if (!productId || !quantity || !userId) {
    return res.status(400).json({
      success: false,
      message: 'product id, quantity, user id required',
    });
  }
  try {
    await models.carts.create({
      product_id: productId,
      user_id: userId,
      quantity,
    });
    return res.status(200).json({
      success: true,
      message: 'Product added in cart',
    });
  } catch (err) {
    if (err.name === 'SequelizeForeignKeyConstraintError') {
      return res.status(400).json({
        success: false,
        message: 'Either user or product does not exist',
      });
    }
    /**
     * in case of 500(because in above code only reason of failure is not able to get data
     * from database) not sending error message to consumer,
     * However, it should be logged into our system for debugging root cause of error
     * console will be replaced by some other logging adding log in grayLog, elastic etc.
     *  */
    console.log(err);
    return res.status(500).json({
      success: false,
      message: 'Some Error Occurred',
    });
  }
};

module.exports = {
  addProcutsInCart,
};
