/* eslint-disable no-restricted-globals */
const bcrypt = require('bcrypt');
const models = require('../server/models');

function validateEmail(mail) {
  // eslint-disable-next-line no-useless-escape
  return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail);
}

const createUser = async (req, res) => {
  const {
    firstName, lastName, address, email, password,
  } = req.body || {};
  const errors = [];
  if (!firstName) {
    errors.push('First Name');
  }
  if (!email) {
    errors.push('Email');
  }
  if (!password) {
    errors.push('Password');
  }
  if (errors.length) {
    return res.status(400).json({
      success: false,
      message: `${errors.join(',')} is/are required`,
    });
  }
  if (!validateEmail(email)) {
    return res.status(400).json({
      success: false,
      message: 'Email is invalid',
    });
  }
  try {
    const hashPassword = await bcrypt.hash(password, 10);
    await models.users.create({
      first_name: firstName,
      last_name: lastName,
      password: hashPassword,
      address,
      email,
    });
    return res.status(200).json({
      success: true,
      message: 'User added',
    });
  } catch (err) {
    /**
     * in case of 500(because in above code only reason of failure is not able
     *  to get data from database) not sending error message to consumer,
     * However, it should be logged into our system for debugging root cause of error
     * console will be replaced by some other logging adding log in grayLog, elastic etc.
     *  */
    console.log(err);
    return res.status(500).json({
      success: false,
      message: 'Some error occured',
    });
  }
};
/**
 * list all carts belonging to a particular user
 * @param {*} req
 * @param {*} res
 */
const listCartsOfUser = async (req, res) => {
  const { userId } = req.params;
  if (isNaN(userId)) {
    return res.status(400).json({
      success: false,
      message: 'Catgory Id should be number',
    });
  }
  try {
    const carts = await models.carts.findAll({
      where: {
        user_id: +userId,
      },
      include: [
        {
          model: models.products,
        },
      ],
    });
    return res.status(200).json({
      carts,
      success: true,
      message: 'Carts List of specific user',
    });
  } catch (err) {
    /**
     * in case of 500(because in above code only reason of failure is not able to
     *  get data from database) not sending error message to consumer,
     * However, it should be logged into our system for debugging root cause of error
     * console will be replaced by some other logging adding log in grayLog, elastic etc.
     *  */
    console.log(err);
    return res.status(500).json({
      success: false,
      message: 'Some Error Occurred',
    });
  }
};
module.exports = {
  listCartsOfUser,
  createUser,
};
