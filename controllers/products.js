/* eslint-disable no-restricted-globals */
const models = require('../server/models');

/**
 * add a new product in products table
 * @param {*} req
 * @param {*} res
 */
const addProduct = async (req, res) => {
  const {
    name, categoryId, description, price, make,
  } = req.body || {};
  const errors = [];
  if (!name) {
    errors.push('Name');
  }
  if (!categoryId) {
    errors.push('Category Id');
  }
  if (!price) {
    errors.push('Price');
  }
  if (!make) {
    errors.push('Make');
  }
  if (errors.length) {
    return res.status(400).json({
      success: false,
      message: `${errors.join(',')} is/are required`,
    });
  }
  try {
    const product = await models.products.create({
      name,
      description,
      price,
      make,
      category_id: categoryId,
    });
    return res.status(200).json({
      product,
      success: true,
      message: 'Product added successfully',
    });
  } catch (error) {
    console.log(error.errors);
    if (error.name === 'SequelizeUniqueConstraintError') {
      return res.status(400).json({
        success: false,
        message: error.errors[0].message,
      });
    }
    /**
     * in case of 500(because in above code only reason of failure is not able to
     *  get data from database) not sending error message to consumer,
     * However, it should be logged into our system for debugging root cause of error
     * console will be replaced by some other logging adding log in grayLog, elastic etc.
     *  */
    console.log(error);
    return res.status(500).json({
      success: false,
      message: 'Some Error Occurred',
    });
  }
};
/**
 * list all/ single products/product depending on request
 * @param {*} req
 * @param {*} res
 */
const listProducts = async (req, res) => {
  try {
    const { productId } = req.params;
    if (productId) {
      if (isNaN(productId)) {
        return res.status(400).json({
          success: false,
          message: 'Product Id should be number',
        });
      }
      let product = await models.products.findOne({
        where: {
          id: +productId,
        },
      });
      if (!product) {
        return res.status(200).json({
          product: {},
          success: true,
          message: 'No product found',
        });
      }
      product = product.get({ plain: true });
      return res.status(200).json({
        product,
        success: true,
        message: 'Product found',
      });
    }
    const products = await models.products.findAll({});
    return res.status(200).json({
      products,
      success: true,
      message: 'Products list',
    });
  } catch (err) {
    /**
     * in case of 500(because in above code only reason of failure is not able to
     * get data from database) not sending error message to consumer,
     * However, it should be logged into our system for debugging root cause of error
     * console will be replaced by some other logging adding log in grayLog, elastic etc.
     *  */
    console.log(err);
    return res.status(500).json({
      success: false,
      message: 'Some Error Occurred',
    });
  }
};

module.exports = {
  addProduct,
  listProducts,
};
