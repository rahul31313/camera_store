/* eslint-disable no-restricted-globals */
const models = require('../server/models');

/**
 * add a new category in categories table
 * @param {*} req
 * @param {*} res
 */
const addCategory = async (req, res) => {
  const { name, type, model } = req.body || {};
  const errors = [];
  if (!name) {
    errors.push('Name');
  }
  if (!type) {
    errors.push('Type');
  }
  if (!model) {
    errors.push('model');
  }
  if (errors.length) {
    return res.status(400).json({
      success: false,
      message: `${errors.join(',')} is/are required`,
    });
  }
  try {
    const category = await models.categories.create({
      name,
      type,
      model,
    });
    return res.status(200).json({
      category,
      success: true,
      message: 'Category added successfully',
    });
  } catch (error) {
    console.log(error.errors);
    if (error.name === 'SequelizeUniqueConstraintError') {
      return res.status(400).json({
        success: false,
        message: error.errors[0].message,
      });
    }
    /**
     * in case of 500(because in above code only reason of failure is not able
     * to get data from database) not sending error message to consumer,
     * However, it should be logged into our system for debugging root cause of error
     * console will be replaced by some other logging adding log in grayLog, elastic etc.
     *  */
    console.log(error);
    return res.status(500).json({
      success: false,
      message: 'Some Error Occurred',
    });
  }
};
/**
 * list all/ single category depending on request
 * @param {*} req
 * @param {*} res
 */
const listCategories = async (req, res) => {
  try {
    const { categoryId } = req.params;
    if (categoryId) {
      if (isNaN(categoryId)) {
        return res.status(400).json({
          success: false,
          message: 'Catgory Id should be number',
        });
      }
      let category = await models.categories.findOne({
        where: {
          id: +categoryId,
        },
      });
      if (!category) {
        return res.status(200).json({
          category: {},
          success: true,
          message: 'No category found',
        });
      }
      category = category.get({ plain: true });
      return res.status(200).json({
        category,
        success: true,
        message: 'Category found',
      });
    }
    const categories = await models.categories.findAll({});
    return res.status(200).json({
      categories,
      success: true,
      message: 'Categories list',
    });
  } catch (err) {
    /**
     * in case of 500(because in above code only reason of failure is not able
     * to get data from database) not sending error message to consumer,
     * However, it should be logged into our system for debugging root cause of error
     * console will be replaced by some other logging adding log in grayLog, elastic etc.
     *  */
    console.log(err);
    return res.status(500).json({
      success: false,
      message: 'Some Error Occurred',
    });
  }
};
/**
 * list all products of a category
 * @param {*} req
 * @param {*} res
 */
const listProductsOfCategory = async (req, res) => {
  const { categoryId } = req.params;
  if (isNaN(categoryId)) {
    return res.status(400).json({
      success: false,
      message: 'Catgory Id should be number',
    });
  }
  try {
    let category = await models.categories.findOne({
      where: {
        id: +categoryId,
      },
      attributes: ['id'],
      include: [
        {
          model: models.products,
        },
      ],
    });
    if (!category) {
      return res.status(200).json({
        category: {},
        success: true,
        message: 'No product found',
      });
    }
    category = category.get({ plain: true });
    return res.status(200).json({
      products: category.products,
      success: true,
      message: 'Products list of a specific category',
    });
  } catch (err) {
    /**
     * in case of 500(because in above code only reason of failure is not able
     * to get data from database) not sending error message to consumer,
     * However, it should be logged into our system for debugging root cause of error
     * console will be replaced by some other logging adding log in grayLog, elastic etc.
     *  */
    console.log(err);
    return res.status(500).json({
      success: false,
      message: 'Some Error Occurred',
    });
  }
};

module.exports = {
  addCategory,
  listCategories,
  listProductsOfCategory,
};
